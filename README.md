# French Historical Monuments

This Angular application shows french historical monuments from a public [french API](https://data.culture.gouv.fr/explore/dataset/liste-des-immeubles-proteges-au-titre-des-monuments-historiques/information/) on a [Leaflet](https://leafletjs.com/) map.
Front-end Web Development M1 - Project

# Installation

```bash
#1. Clone the repository
git clone https://gitlab.com/syl.alf999/french-historical-monuments.git

#2. Go to project file
cd french-historical-monuments

#3. Install NPM packages
npm install
```
