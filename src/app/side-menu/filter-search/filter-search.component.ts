import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import { Type } from 'src/app/shared/type.enum';
import {SearchService} from '../../search.service';

@Component({
  selector: 'app-filter-search',
  templateUrl: './filter-search.component.html',
  styleUrls: ['./filter-search.component.css']
})
export class FilterSearchComponent implements OnInit {
  selectedRegion = 'undefined';
  selectedType: string;

  regions: string[] = [ 'Île-de-France', 'Auvergne-Rhône-Alpes', 'Bourgogne-Franche-Comté',
    'Bretagne', 'Centre-Val de Loire', 'Corse', 'Grand Est', 'Hauts-de-France',
    'Normandie', 'Nouvelle-Aquitaine', 'Occitanie', 'Pays de la Loire', 'Provence-Alpes-Côte d\'Azur'
  ];

  monumentTypes: string[] = ['Musée', 'Eglise', 'Hopital', 'Château'];

  constructor(private searchService: SearchService) {
    switch (searchService.getLastSelectedType()){
      case Type.Museum:
        this.selectedType = 'Musée';
        break;
      case Type.Church:
        this.selectedType = 'Eglise';
        break;
      case Type.Castle:
        this.selectedType = 'Château';
        break;
      case Type.Hospital:
        this.selectedType = 'Hopital';
        break;
      case Type.Generic:
      default:
        this.selectedType = Type.Generic.toString();
    }
    
  }

  ngOnInit(): void {}

  

  /**
   * Is triggered when the user click on a type
   */
  onTypeSelect(): void{
    console.log('Selected', this.selectedRegion, this.selectedType);
    this.searchService.setLastSelectedType(this.selectedType);
    this.searchService.filterQuery(this.selectedRegion, this.selectedType);
    
  }

}
