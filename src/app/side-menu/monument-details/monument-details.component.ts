import { Component, OnInit } from '@angular/core';
import {Monument} from '../../shared/monument.model';
import {MonumentService} from '../../monument.service';

@Component({
  selector: 'app-monument-details',
  templateUrl: './monument-details.component.html',
  styleUrls: ['./monument-details.component.css']
})
export class MonumentDetailsComponent implements OnInit {
  monument: Monument;

  isFavorite: boolean; // binding for button
  constructor(private monumentService: MonumentService) { }

  ngOnInit(): void {
    this.monumentService.onMonumentDetailsRequested.subscribe((monument: Monument) => {
      this.monument = monument;
      this.isFavorite = monument.isFavorite;
    });
  }
  onFavorite(): void{
    this.isFavorite =! this.isFavorite;
    this.monument.isFavorite = this.isFavorite;
    this.monumentService.saveAsFavorite(this.monument);
  }
  WikiLinkClick(): void{
    window.open(`https://www.google.fr/search?q=${this.monument.name} wiki`, '_blank');
  }
}
