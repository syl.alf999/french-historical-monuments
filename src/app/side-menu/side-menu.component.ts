import { Component, OnInit} from '@angular/core';
import {MonumentService} from '../monument.service';
import {Monument} from '../shared/monument.model';
import {SearchService} from '../search.service';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit {
  userQuery = '';
  suggestions = [];


  constructor(private searchService: SearchService, private monumentService: MonumentService) { }

  ngOnInit(): void {
    this.searchService.onSuggestionQuery
    .subscribe((ticos: string[]) => {
        this.suggestions = ticos;
      });

  }

  /**
   * Send user query
   */
  onSearchQuerySent(): void{
    this.suggestions = []
    this.searchService.simpleSearch(this.userQuery);
  }

  clickSuggestion(text: string): void{
    this.userQuery = text;
    this.onSearchQuerySent();
  }

  updateSuggestion(text: string){
  
    console.log("update suggestions: " + text);
    if(text == ""){
      this.suggestions = []
      return;
    }
    
    //this.searchService.getSuggestions(this.userQuery, this.suggestions);

    this.searchService.getSuggestions(this.userQuery);

    console.log("suggestion: " + this.suggestions.toString());
  

  }
  
  

}
