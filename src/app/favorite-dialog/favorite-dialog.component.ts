import { Component, OnInit } from '@angular/core';
import {MonumentService} from '../monument.service';
import {Monument} from '../shared/monument.model';

@Component({
  selector: 'app-favorite-dialog',
  templateUrl: './favorite-dialog.component.html',
  styleUrls: ['./favorite-dialog.component.css']
})
export class FavoriteDialogComponent implements OnInit {
  favoriteMonuments: Monument[] = [];

  constructor(private monumentService: MonumentService) { }

  ngOnInit(): void {
   this.favoriteMonuments = this.monumentService.favoriteMonuments;

   this.monumentService.onNewFavoriteMonument.subscribe(() => {
     this.favoriteMonuments = this.monumentService.favoriteMonuments;
   });
  }

}
