import { Component } from '@angular/core';
import {MonumentService} from './monument.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [MonumentService]
})
export class AppComponent {
  constructor() {}
}
