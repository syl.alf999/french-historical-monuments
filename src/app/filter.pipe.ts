import { Pipe, PipeTransform } from '@angular/core';
import { MapComponent } from './map/map.component';
import { SideMenuComponent } from './side-menu/side-menu.component';

@Pipe({ name: 'appFilter' })
export class FilterPipe implements PipeTransform {

  transform(items: any[], searchText: string): any[] {
    if (!items) {
      return [];
    }
    if (!searchText) {
      return items;
    }
    searchText = searchText.toLocaleLowerCase();

    

    return items.filter(it => {
      return it.toLocaleLowerCase().includes(searchText);
    });
  }
}
