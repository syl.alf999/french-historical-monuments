import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { MatSidenavModule } from '@angular/material/sidenav';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatDialogModule} from '@angular/material/dialog';
import {MatListModule} from '@angular/material/list';


import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SideMenuComponent } from './side-menu/side-menu.component';
import { MonumentDetailsComponent } from './side-menu/monument-details/monument-details.component';
import { FilterSearchComponent } from './side-menu/filter-search/filter-search.component';
import {MonumentService} from './monument.service';
import {SearchService} from './search.service';
import { FilterPipe } from './filter.pipe';
import { HighlightDirective } from './highlight.directive';
import { FavoriteDialogComponent } from './favorite-dialog/favorite-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MapComponent,
    SideMenuComponent,
    MonumentDetailsComponent,
    FilterSearchComponent,
    FilterPipe,
    HighlightDirective,
    FavoriteDialogComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatSidenavModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatToolbarModule,
    MatCardModule,
    MatDividerModule,
    MatSelectModule,
    MatButtonToggleModule,
    MatSnackBarModule,
    MatDialogModule,
    MatListModule
  ],
  providers: [MonumentService, SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
