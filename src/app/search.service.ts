import { Injectable, EventEmitter} from '@angular/core';
import {MonumentService} from './monument.service';
import {Monument} from './shared/monument.model';
import { Type } from './shared/type.enum';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  onSimpleQuery = new EventEmitter<Monument[]>();
  onFilteredQuery = new EventEmitter<Monument[]>();
  onSuggestionQuery = new EventEmitter<string[]>();

  public selectedType:string;

  constructor(private monumentService: MonumentService) {  }

  simpleSearch(userQuery: string): void{
    console.log(userQuery);

    this.monumentService.getMonuments(userQuery)
      .subscribe((monuments: Monument[]) => {
        this.onSimpleQuery.emit(monuments);
      });

  }

  getSuggestions(userQuery: string): void{
    /*let monu = this.monumentService.getSuggestions(userQuery, "&rows=5");
    console.log("monu: " + monu);
    let sugg = [];
    for (const monument in monu){
      sugg.push(monument);
    }
    return sugg;*/

    

    this.monumentService.getSuggestions(userQuery, "&rows=5")
    .subscribe((ticos: string[])=>{
        this.onSuggestionQuery.emit(ticos);
    });

    

  }

  filterQuery(selectedRegion: string, selectedType: string): void{
    let urlEndPoint = '';
    if (selectedRegion !== 'undefined'){
      urlEndPoint = '&q=(reg:"' + encodeURI(selectedRegion) + '" AND tico:' + encodeURI(selectedType) + ')';
    }
    else{
      urlEndPoint = '&q=(tico:' + encodeURI(selectedType) + ')';
    }


    this.monumentService.getMonuments('', urlEndPoint, selectedType)
      .subscribe((monuments: Monument[]) => {
        this.onFilteredQuery.emit(monuments);
      });
  }

  getLastSelectedType():Type{
    return this.monumentService.getLastSeenTypeAsType();
  }
  setLastSelectedType(type:string){
    this.selectedType=type;
    console.log(this.selectedType);
    
  }
}
