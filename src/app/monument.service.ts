import {EventEmitter, Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Monument} from './shared/monument.model';
import {Type} from './shared/type.enum';

@Injectable({
  providedIn: 'root'
})
export class MonumentService{
  private baseUrl = 'https://data.culture.gouv.fr/api/records/1.0/search/?dataset=liste-des-immeubles-proteges-au-titre-des-monuments-historiques';
  onMonumentDetailsRequested = new EventEmitter<Monument>();
  onNewFavoriteMonument = new EventEmitter<void>();
  public favoriteMonuments: Monument[];

  constructor(private http: HttpClient) {
    this.favoriteMonuments = this.loadAllFavoriteMonumentID();
  }

  /**
   * Call API to get monuments
   */
  getMonuments(monumentNameQuery: string, filter: string = '', selectedType: string = '', recordIDBased: boolean= false): any{
    let urlToCall: string;
    let urlQuery: string;
    if (!recordIDBased){
      urlQuery = '&q=(tico:';
    }
    else{
      urlQuery = '&q=(recordid:';
    }

    urlToCall = (filter.length !== 0) ? this.baseUrl + filter : this.baseUrl + urlQuery + encodeURI(monumentNameQuery) + ')&rows=15';

    return this.http.get(urlToCall)
      .pipe(map(responseData => {
        const rawMonuments = [];
        const recordsKey = 'records';

        for (const key in responseData){
          if (responseData.hasOwnProperty(key) && key === recordsKey){
            responseData[key].forEach(row => {

              // Fetch necessary attributes of a monument
              const recordid: string = row.recordid;
              const name: string = row.fields.tico;
              const commune: string = row.fields.commune;
              const zipCode: string = row.fields.insee;
              const region: string = row.fields.reg;
              const coordinates: number[] = row.fields.coordonnees_ban;
              const ppro: string = row.fields.ppro;
              const hist = row.fields.hasOwnProperty('hist') ? row.fields.hist : '';
              const type = this.getSelectedType(selectedType);
              const favorite = this.loadFavorite(recordid);
              // Add fetched monument to the results array
              const monument: Monument = new Monument(recordid, name, commune, zipCode, region, coordinates, ppro, hist, type, favorite);
              rawMonuments.push(monument);
            });
          }
        }

        return rawMonuments;
      }));
  }

  getSuggestions(query: string, filter: string = '', selectedType: string = ''): any{
    const urlToCall =  this.baseUrl + filter + '&q=' + encodeURI(query);
    console.log(urlToCall);



    return this.http.get(urlToCall)
      .pipe(map(responseData => {
        const ticos = [];
        const recordsKey = 'records';

        for (const key in responseData){
          if (responseData.hasOwnProperty(key) && key === recordsKey){
            responseData[key].forEach(row => {

              ticos.push( row.fields.tico);
            });
          }
        }
        return ticos;
      }));
  }

  displayMonument(monument: Monument, selectedType: string): void{
    // save the monument as the last seen one
    this.saveLastSeenMonument(monument, this.getSelectedType(selectedType));
    this.onMonumentDetailsRequested.emit(monument);
  }

  saveAsFavorite(monument: Monument): void{
    localStorage.setItem(monument.recordid, (monument.isFavorite ? '1' : '0'));
    this.favoriteMonuments.push(monument);
    this.onNewFavoriteMonument.emit();
  }
  loadFavorite(monumentID: string): boolean{
    return (localStorage.getItem(monumentID) === '1') ? true : false;
  }

  /**
   * Fetch the list of the user favorite monuments ID
   */
  loadAllFavoriteMonumentID(): Monument[]{
    const localStorageKeys = Object.keys(localStorage);
    const monuments: Monument[] = [];
    localStorageKeys.forEach(element => {
      if (element !== 'lastSeenSelectedType' && element !== 'lastSeenMonument'){
        this.getMonuments(element, '', '', true).subscribe((m: Monument[]) => {
          monuments.push(m[0]);
        });
      }
    });
    return monuments;
  }

  getSelectedType(selectedType: string): Type{
    switch (selectedType){
      case 'Musée':
      case Type.Museum.toString():
        return Type.Museum;
        break;
      case 'Eglise':
      case Type.Church.toString():
        return Type.Church;
        break;
      case 'Château':
      case Type.Castle.toString():
        return Type.Castle;
        break;
      case 'Hopital':
      case Type.Hospital.toString():
        return Type.Hospital;
        break;
      case Type.Generic.toString():
      default:
        return Type.Generic;
    }
  }

  saveLastSeenMonument(monument: Monument, monumentType: Type): void{
    localStorage.setItem('lastSeenSelectedType', monumentType.toString());
    localStorage.setItem('lastSeenMonument', monument.recordid);
  }
  getLastSeenType(): string{
    // check if there is a saved last monument seen type, if not, return the generic one
    return (localStorage.getItem('lastSeenSelectedType') != null) ? localStorage.getItem('lastSeenSelectedType') : '4';
  }
  getLastSeenTypeAsType(): Type{
    // check if there is a saved last monument seen type, if not, return the generic one. this version returns as Type instead of string
    return this.getSelectedType((localStorage.getItem('lastSeenSelectedType') != null) ? localStorage.getItem('lastSeenSelectedType') : '4');
  }
  getLastSeenMonument(): string{
    // check if there is a saved last monument seen, if not, return a default one
    return (localStorage.getItem('lastSeenMonument') != null) ? localStorage.getItem('lastSeenMonument') : 'arc de triomphe';
  }
}
