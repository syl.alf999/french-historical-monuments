export enum Type {
  Church,
  Museum,
  Hospital,
  Castle,
  Generic
}
