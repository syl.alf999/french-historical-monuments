import {Type} from './type.enum';

export class Monument{
  private _recordid: string; // fields.recordid
  private _name: string; // fields.tico
  private _commune: string; // fields.commune
  private _zipCode: string; // fields.insees
  private _region: string; // fields.reg
  private _coordinates: number[]; // geometry.coordinates
  private _ppro: string; // fields.ppro descripttion with inscription date
  private _hist: string; // fields.hist
  private _type: Type;
  private _isFavorite: boolean;

  /**
   *
   * @param recordid
   * @param name
   * @param commune
   * @param zipCode
   * @param region
   * @param coordinates
   * @param ppro
   * @param hist
   * @param isFavorite
   */
    constructor(recordid: string, name: string, commune: string, zipCode: string,
                region: string, coordinates: number[], ppro: string, hist: string, type: Type, isFavorite:boolean ){
        this._recordid = recordid;
        this._name = name;
        this._commune = commune;
        this._zipCode = zipCode;
        this._region = region;
        this._coordinates = coordinates;
        this._ppro = ppro;
        this._hist = hist;
        this._type = type;
        this._isFavorite=isFavorite;
    }

    getFullAddress(): string{
      return this.commune + ' ' + this.zipCode + ' ' + this.region;
    }

  get recordid(): string{
      return this._recordid;
  }

  get name(): string {
    return this._name;
  }

  get commune(): string {
    return this._commune;
  }

  get zipCode(): string {
    return this._zipCode;
  }

  get region(): string {
    return this._region;
  }

  get coordinates(): number[] {
    return this._coordinates;
  }

  get ppro(): string{
    return this._ppro;
  }

  get hist(): string{
    return this._hist;
  }

  get type(): Type {
    return this._type;
  }

  get isFavorite():boolean{
    return this._isFavorite;
  }

  set isFavorite(state:boolean){
    this._isFavorite=state;
  }
}
