import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import * as L from 'leaflet';
import {Circle, Icon, LatLng, LayerGroup, LeafletEvent, Marker, TileLayer} from 'leaflet';
import {Monument} from '../shared/monument.model';
import {MonumentService} from '../monument.service';
import {SearchService} from '../search.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Type} from '../shared/type.enum';
import {MatDialog} from '@angular/material/dialog';
import {FavoriteDialogComponent} from '../favorite-dialog/favorite-dialog.component';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit, OnInit {
  private map: L.Map;
  private layerGroup: LayerGroup; // layer containing the markers
  private favorite: Monument[] = [];

  private userLayerGroup: LayerGroup; // layer containing element about the user
  userCircle: Circle;
  userMarker: Marker;

  hasAllowedLocalisationAccess = false;

  isUserLocationDiplayed = true;
  userLocationStatus = 'Cacher ma position';

  // All icons will inherit these properties
  MonumentIcon = Icon.extend({
    options: {
      iconSize: [50, 50], // size of the icon
      iconAnchor: [25, 50], // point of the icon which will correspond to marker's location
      popupAnchor: [0, -50] // point from which the popup should open relative to the iconAnchor
    }
  });

  // @ts-ignore
  private userIcon = new this.MonumentIcon({iconUrl: 'assets/leaflet/user.svg'});
  // @ts-ignore
  private museumIcon = new this.MonumentIcon({iconUrl: 'assets/leaflet/museum.svg'});
  // @ts-ignore
  private hospitalIcon = new this.MonumentIcon({iconUrl: 'assets/leaflet/hospital.svg'});
  // @ts-ignore
  private churchIcon = new this.MonumentIcon({iconUrl: 'assets/leaflet/church.svg'});
  // @ts-ignore
  private castleIcon = new this.MonumentIcon({iconUrl: 'assets/leaflet/castle.svg'});
  // @ts-ignore
  private genericIcon = new this.MonumentIcon({iconUrl: 'assets/leaflet/mountain.svg'});


  private monuments: Monument[] = []; // array containing the current monuments fetched
  @ViewChild('mapElement') public mapElement: ElementRef<HTMLDivElement>;

  constructor(private monumentService: MonumentService, private searchService: SearchService, private snackBar: MatSnackBar,
              public dialog: MatDialog) { }

  ngOnInit(): void {

    // Load monument the first time the user is on the web site
    this.monumentService.getMonuments(this.monumentService.getLastSeenMonument(), '', this.monumentService.getLastSeenType(), true)
      .subscribe((monuments: Monument[]) => {
        this.checkIfNoMonumentResults(monuments);
        if (monuments.length === 1) {this.monumentService.displayMonument(monuments[0], this.monumentService.getLastSeenType()); }
      });

    // Reload map layout with new markers from the user simple search query
    this.searchService.onSimpleQuery
      .subscribe((monuments: Monument[]) => {
          this.checkIfNoMonumentResults(monuments);
        });

    // Reload map layout with new markers from filtered query
    this.searchService.onFilteredQuery
      .subscribe((monuments: Monument[]) => {
        this.checkIfNoMonumentResults(monuments);
      });


    // Get user localization
    this.getUserLocalizationPermission();
  }

  ngAfterViewInit(): void {
    this.initializeMap();
  }

  /**
   * Initialize an OpenStreetMap centered on France
   */
  initializeMap(): void{
    this.map = new L.Map(this.mapElement.nativeElement, {
      center: [46.82786, 2.68066],
      zoom: 6
    });

    const tiles = new TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      minZoom: 6,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    tiles.addTo(this.map);
    this.layerGroup = new LayerGroup().addTo(this.map);
  }

  /**
   * Add markers on the layer of the map only if the array of monument is not empty
   * or else an error is shown
   */
  checkIfNoMonumentResults(monuments: Monument[]): void{
    if (monuments.length === 0){
      this.openSnackBar('Aucun résultat !');
    }
    else{
      this.monuments = monuments;
      this.makeMarkers();
    }
  }

  /**
   * Ask permission to the user to access to his location
   */
  getUserLocalizationPermission(): void {
    navigator.permissions.query({name: 'geolocation'}).then((result) => {
      if (result.state === 'prompt') {
        navigator.geolocation.getCurrentPosition(position => {
          this.createUserMarkerAndPerimeter(new LatLng(position.coords.latitude, position.coords.longitude));
          this.hasAllowedLocalisationAccess = true;
          // console.log(position.coords.latitude, position.coords.longitude);
        });
      }
    });
  }

  /**
   * Add user marker on the map according to his location
   * Draw a circle of 10km around him
   */
  createUserMarkerAndPerimeter(latlng: LatLng): void{
    this.userLayerGroup = new LayerGroup().addTo(this.map);

    this.userCircle =  new Circle(latlng, {
      color: '#0A71EF',
      fillColor: '#789beb',
      fillOpacity: 0.2,
      radius: 10000
    }).addTo(this.userLayerGroup);

    this.userMarker = new Marker(latlng, {icon: this.userIcon})
      .bindPopup('Votre position')
      .addTo(this.userLayerGroup);

  }

  /**
   * Hide or show user location
   */
  toggleUserPositionButton(): void{
    this.isUserLocationDiplayed = !this.isUserLocationDiplayed;

    if (this.isUserLocationDiplayed){
      this.userLocationStatus = 'Cacher ma position';
      this.map.addLayer(this.userLayerGroup);
    }
    else {
      this.userLocationStatus = 'Montrer ma position';
      this.map.removeLayer(this.userLayerGroup);
    }
  }

  /**
   * Place markers on the map
   */
  makeMarkers(): void{
    this.resetMarkers();

    for (const monument of this.monuments) {
      // Add icon and a click event to the marker
      new Marker(new LatLng(monument.coordinates[0], monument.coordinates[1]), this.selectIcon(monument))
        .bindPopup(monument.name)
        .addTo(this.layerGroup)
        .on('click', this.getMonumentClicked.bind(this));
    }
  }

  /**
   * Select the right icon of the marker
   */
  selectIcon(monument: Monument): {icon: Icon}{
    switch (monument.type){
      case Type.Museum:
        return {icon: this.museumIcon};
        break;
      case Type.Church:
        return {icon: this.churchIcon};
        break;
      case Type.Castle:
        return {icon: this.castleIcon};
        break;
      case Type.Hospital:
        return {icon: this.hospitalIcon};
        break;
      default:
        return {icon: this.genericIcon};
    }
  }

  /**
   * Remove all existing markers from the map
   */
  resetMarkers(): void{
    this.layerGroup.clearLayers();
    this.layerGroup = new LayerGroup().addTo(this.map);
  }

  /**
   * Show error message
   */
  openSnackBar(message: string): void {
    this.snackBar.open(message, 'Fermer', {
      duration: 3000,
    });
  }

  /**
   * Search for the monument clicked
   */
  getMonumentClicked(e: LeafletEvent): void{
    // Casting lat and lng to number with + sign
    const lat = +e.target.getLatLng().lat;
    const lng = +e.target.getLatLng().lng;

    // Fetch the monument with the latitude and longitude clicked
    const monumentClicked = this.monuments.filter(x => x.coordinates[0] === lat && x.coordinates[1] === lng)[0];
    this.monumentService.displayMonument(monumentClicked, this.searchService.selectedType);
  }

  seeFavoriteMonuments(): void{
    this.dialog.open(FavoriteDialogComponent);
  }

}
